package io.github.austerzockt.luckpermsrankgui.events;

import de.tr7zw.changeme.nbtapi.NBTItem;
import io.github.austerzockt.luckpermsrankgui.LuckpermsRankGUI;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.NodeType;
import net.luckperms.api.node.types.InheritanceNode;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.time.Instant;
import java.util.UUID;

public class ClickListener implements Listener {
    private LuckpermsRankGUI luckpermsRankGUI;

    public ClickListener(LuckpermsRankGUI luckpermsRankGUI) {
        this.luckpermsRankGUI = luckpermsRankGUI;
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (event.getCurrentItem() == null)
            return;
        if (event.getCurrentItem().getType().equals(Material.RED_STAINED_GLASS_PANE)) {
            NBTItem item = new NBTItem(event.getCurrentItem());
            if (item.hasKey("rankgui_target") && item.hasKey("rankgui_operator")
                    && item.hasKey("rankgui_rank")) {
                String target = item.getString("rankgui_target");
                String operator = item.getString("rankgui_operator");
                String rank = item.getString("rankgui_rank");
                User user = luckpermsRankGUI.getLuckPerms().getUserManager()
                        .getUser(UUID.fromString(target));
                user.getNodes(NodeType.INHERITANCE).forEach(user.data()::remove);
                user.data().add(InheritanceNode.builder().group(rank)
                        .build());
                luckpermsRankGUI.getLuckPerms().getUserManager().saveUser(user);
                event.getView().close();
                event.setCancelled(true);
                var oper_uuid = UUID.fromString(operator);
                var target_uuid = UUID.fromString(target);
                var action = luckpermsRankGUI.getLuckPerms().getActionLogger().actionBuilder().source(oper_uuid)
                        .sourceName(Bukkit.getOfflinePlayer(oper_uuid).getName())
                        .target(user.getUniqueId())
                        .description("(GUI) parent set " + rank)
                        .targetName(Bukkit.getOfflinePlayer(target_uuid).getName())
                        .targetType(net.luckperms.api.actionlog.Action.Target.Type.USER)
                        .timestamp(Instant.now()).build();

                luckpermsRankGUI.getLuckPerms().getActionLogger().submit(action);
            }
        }
    }
}