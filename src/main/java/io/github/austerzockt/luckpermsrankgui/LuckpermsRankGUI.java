package io.github.austerzockt.luckpermsrankgui;

import io.github.austerzockt.luckpermsrankgui.command.RankCommand;
import io.github.austerzockt.luckpermsrankgui.config.ConfigManager;
import io.github.austerzockt.luckpermsrankgui.events.ClickListener;
import io.github.austerzockt.luckpermsrankgui.utils.LuckPermsUtils;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class LuckpermsRankGUI extends JavaPlugin implements Listener {
    public static LuckPermsUtils luckPermsUtils;
    private static ConfigManager configManager;

    public LuckPerms getLuckPerms() {
        return LuckPermsProvider.get();
    }

    public LuckPermsUtils getLuckPermsUtils() {
        return luckPermsUtils;
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }

    @Override
    public void onEnable() {
        configManager = new ConfigManager(this);
        configManager.register("config");
        luckPermsUtils = new LuckPermsUtils(this);
        registerCommand("rank", new RankCommand(this), true);
        Bukkit.getPluginManager().registerEvents(new ClickListener(this), this);
    }

    private void registerCommand(String command, CommandExecutor executor, boolean hasTab) {
        this.getCommand(command).setExecutor(executor);
        if (hasTab) this.getCommand(command).setTabCompleter((TabCompleter) executor);
    }

    public Material getItem(String rank) {
        ConfigurationSection section = configManager.get("config").getConfigurationSection("rankItems");
        if (section.getKeys(false).contains(rank)) {
            return Material.getMaterial(section.getConfigurationSection(rank).getString("item"));
        } else {
            return Material.getMaterial(configManager.get("config").getString("defaultItem"));
        }
    }

    public String getName(String rank) {
        ConfigurationSection section = configManager.get("config").getConfigurationSection("rankItems");
        if (section.getKeys(false).contains(rank)) {
            return section.getConfigurationSection(rank).getString("name");
        }
        return StringUtils.capitalize(rank);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equalsIgnoreCase("luckpermsrankgui")) {
            if (sender.hasPermission("luckpermsrankgui.use")) {
                sender.sendMessage(ChatColor.GOLD + "Using LuckPerms-RankGUI v" + this.getDescription().getVersion() + " by " + this.getDescription().getAuthors().stream().reduce((s, s1) -> s + ", " + s1).orElse("Auster"));
            }
        }
        return true;
    }
}


