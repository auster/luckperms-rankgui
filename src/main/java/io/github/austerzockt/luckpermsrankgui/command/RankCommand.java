package io.github.austerzockt.luckpermsrankgui.command;

import com.google.common.collect.Lists;
import de.tr7zw.changeme.nbtapi.NBTItem;
import io.github.austerzockt.luckpermsrankgui.LuckpermsRankGUI;
import net.luckperms.api.model.user.User;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class RankCommand implements CommandExecutor, TabCompleter {
    private LuckpermsRankGUI luckpermsRankGUI;

    public RankCommand(LuckpermsRankGUI luckpermsRankGUI) {
        this.luckpermsRankGUI =
                luckpermsRankGUI;
    }


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equalsIgnoreCase("rank")) {

            if (sender.hasPermission("luckpermsrankgui.use")) {
                if (sender instanceof Player) {
                    Player p = (Player) sender;
                    if (args.length == 1) {
                        Player target = Bukkit.getPlayer(args[0]);
                        if (target == null) {
                            sender.sendMessage(ChatColor.RED + "Unknown Player!");
                            return true;
                        }
                        if (!luckpermsRankGUI.getLuckPermsUtils().canAccess(p, target, luckpermsRankGUI.getConfigManager().get("config")
                                .getBoolean("setSameGroup"))) {
                            p.sendMessage(ChatColor.RED
                                    + "You cannot set this players group");
                            return true;
                        }
                        p.openInventory(createInventory(target, p,
                                p.hasPermission("luckpermsrankgui.setownhighest")));
                    } else {
                        sender.sendMessage(ChatColor.RED + "/rank [Player]");
                    }
                }
            }
        }
        return true;
    }

    @Override
    public java.util.List<String> onTabComplete(CommandSender sender, Command command, String alias,
                                                String[] args) {
        if (command.getName().equals("rank")) {
            ArrayList<String> list = Lists.newArrayList();
            StringUtil.copyPartialMatches(args[0], Bukkit.getOnlinePlayers().stream().map(HumanEntity::getName)
                    .collect(Collectors.toList()), list);
            return list;
        } else
            return null;
    }

    public Inventory createInventory(Player target, Player operator, boolean canSetOwnHighest) {
        var groups = luckpermsRankGUI.getLuckPerms().getGroupManager().getLoadedGroups();
        operator.sendMessage(canSetOwnHighest + "");
        Inventory inventory = Bukkit.createInventory(null, 9 * calculateInventorySize(groups.size()), target.getDisplayName());

        User lp_operator = luckpermsRankGUI.getLuckPerms().getPlayerAdapter(Player.class).getUser(operator);
        for (var group : luckpermsRankGUI.getLuckPermsUtils().getGroups(luckpermsRankGUI.getLuckPermsUtils().getHighestGroup(lp_operator), !canSetOwnHighest)) {

            ItemStack stack = new ItemStack(luckpermsRankGUI.getItem(group.getName()));
            ItemMeta meta = stack.getItemMeta();
            meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', luckpermsRankGUI.getName(group.getName())));
            ArrayList<String> lore = Lists.newArrayList();
            String prefix = group.getCachedData().getMetaData().getPrefix();
            lore.add(ChatColor.WHITE + "Prefix: "
                    + (prefix != null ? ChatColor.translateAlternateColorCodes('&', prefix) : "None"));
            lore.add(ChatColor.WHITE + "Weight: " + group.getWeight().orElse(-1));
            meta.setLore(lore);
            stack.setItemMeta(meta);
            NBTItem nbtitem = new NBTItem(stack, true);
            nbtitem.setString("rankgui_target", target.getUniqueId().toString());
            nbtitem.setString("rankgui_rank", group.getName());
            nbtitem.setString("rankgui_operator", operator.getUniqueId().toString());
            inventory.addItem(stack);

        }
        return inventory;
    }

    public int calculateInventorySize(int size) {
        return size / 9 + (size % 9 == 0 ? 0 : 1);
    }
}
