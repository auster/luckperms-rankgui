package io.github.austerzockt.luckpermsrankgui.utils;

import com.google.common.collect.Lists;
import io.github.austerzockt.luckpermsrankgui.LuckpermsRankGUI;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.model.group.Group;
import net.luckperms.api.model.user.User;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class LuckPermsUtils {
    private final LuckpermsRankGUI luckpermsrankgui;
    private LuckPerms luckperms;

    public LuckPermsUtils(LuckpermsRankGUI luckpermsrankgui) {
        this.luckpermsrankgui = luckpermsrankgui;
        this.luckperms = LuckPermsProvider.get();
    }

    public boolean canAccess(Player player, Player target, boolean setSameGroup) {
        return canAccess(luckperms.getPlayerAdapter(Player.class).getUser(player),
                luckperms.getPlayerAdapter(Player.class).getUser(target), setSameGroup);
    }

    public boolean canAccess(User user, User target, boolean setSameGroup) {
        if (setSameGroup)
            return getHighestGroup(user).getWeight().orElseThrow() >= getHighestGroup(target).getWeight()
                    .orElseThrow();
        return getHighestGroup(user).getWeight().orElseThrow() > getHighestGroup(target).getWeight()
                .orElseThrow();
    }

    public List<Group> getGroups(Group group, boolean below) {
        List<Group> groups = new ArrayList<>(luckperms.getGroupManager().getLoadedGroups());
        List<Group> output = Lists.newArrayList();
        for (var g : groups) {
            if (below) {
                if (g.getWeight().orElse(0) < group.getWeight().orElse(0)) {
                    output.add(g);
                    Bukkit.broadcastMessage(g.getName() + " for " + group.getName());
                }
            } else {
                if (g.getWeight().orElse(0) <= group.getWeight().orElse(0)) {
                    output.add(g);
                    Bukkit.broadcastMessage(g.getName() + " for " + group.getName());
                }
            }
        }
        return sortList(output);
    }

    private List<Group> sortList(List<Group> list) {
        list.sort(Comparator.comparingLong(g -> g.getWeight().orElseThrow()));
        return list;
    }

    public Group getHighestGroup(User user) {
        var groups = new ArrayList<>(user.getInheritedGroups(user.getQueryOptions()));
        Bukkit.getLogger().severe(groups.get(0).getName());
        if (groups.size() == 1)
            return groups.get(0);
        Group group = null;
        for (Group g : groups) {
            if (group == null) {
                group = g;
                continue;
            }
            if (g.getWeight().orElse(0) > group.getWeight().orElse(0))
                group = g;
            Bukkit.broadcastMessage("Setting " + (group == null ? "null" : group.getName()) + "-> "
                    + group.getName());

        }

        Bukkit.getLogger().severe(group.getName());
        return group != null ? group : luckperms.getGroupManager().getGroup("default");
    }
}
