package io.github.austerzockt.luckpermsrankgui.config;

import java.util.List;

import com.google.common.collect.Lists;

import org.bukkit.plugin.java.JavaPlugin;

public class ConfigManager {

	private JavaPlugin plugin;
	private List<MyConfig> configs;

	public ConfigManager(JavaPlugin plugin) {
		this.plugin = plugin;
		configs = Lists.newArrayList();
	}

	public void register(String name) {
		configs.add(new MyConfig(plugin, name));
	}

	public MyConfig get(String name) {
		return configs.stream().filter(s -> s.getName().equals(name)).findFirst().orElse(null);
	}

	public List<MyConfig> getConfigs() {
		return configs;
	}
}
