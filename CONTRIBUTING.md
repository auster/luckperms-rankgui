There are no real guidelines to contribute right now. If you find that this plugin lacks any feature you want, either open an issue or, even better, a PR with the feature.
