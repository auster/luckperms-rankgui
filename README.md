This Plugin is a GUI for the "/luckperms user [Name] parent set [group]" command. 

It supports weight-based authorisation (due to that you need a working weight system, otherwise everything will be broken without warning).
You cannot set a players group who has higher weight than you by default. (e.g. admin can't demote or promote admin).
You also can only set other players to groups below you (or if you have the permission "luckpermsrankgui.setownhighest" you can set them to your group, but not higher than that).
There is an option in the config to allow players to set the group of players in their own group. I strongly discourage using it, as one rogue admin could demote every other admin on the server.
